# OpenCart 3 中文免费版

> OpenCart 3 中文免费版，是基于目前流行的 `PHP` 开源电商系统 [OpenCart v3.0 官方原版](http://github.com/opencart/opencart)，结合国内电商网站特点进行了大量二次开发和功能优化。目前代码主要由 [opencart.cn](http://www.opencart.cn) 进行维护。欢迎大家关注我们（点右上角的 `Star`）和提交代码！

## zip 安装包下载（普通用户看这里）
我们会不定期更新安装包，安装包已预装了 `composer` 组件和预编译了 scss 等资源。适合非开发者用户。请直接下载 zip 包，解压就可以进行安装了。

**zip 安装包下载地址**：https://www.opencart.cn/forum.php?mod=viewthread&tid=10793&fromuid=1

## 开发者正确安装姿势
开发者请按以下方式步骤进行操作：

1. 命令行中执行 `git pull` 代码至本地
2. `cd` 至开发目录
2. 执行 `composer install` 安装必要的 composer 组件
3. 最后在浏览器中执行 `install` 就可以安装了

## OpenCart 交流群
欢迎大家加入 QQ 群进行功能使用和开发交流。

QQ群1: `112078291`

## Bug 反馈
如果你发现了`八阿哥 (bug)`，你可以提个 issue 告知我们：https://gitee.com/opencartcn/OpenCart3/issues

### 提问的艺术
- 文字要描述清楚，说清楚在哪个页面，你做了什么操作。（千万不要就一句话 `怎么安装不了？` 就完了。我们表示 `尴尬脸.gif`）
- 有截图就最好了
- 一个 `issue` 就说一个问题，反正多提几个 issue 又不要钱

## 有好的建议？
如果你有有好的想法，但不会写代码，欢迎提 issue。如果你是开发者，来提交个代码？

## 功能演示
![首页](https://www.opencart.cn/public/image/gif/free-30/1.gif "首页")

![商品分类](https://www.opencart.cn/public/image/gif/free-30/2.gif "商品分类")

![商品详情](https://www.opencart.cn/public/image/gif/free-30/3.gif "商品详情")

![购物车](https://www.opencart.cn/public/image/gif/free-30/4.gif "购物车")